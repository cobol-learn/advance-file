       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MEMBER-REPORT.
       AUTHOR. PAKAWAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MEMBER-FILE ASSIGN TO "member.dat"
              ORGANIZATION   IS LINE  SEQUENTIAL.
           SELECT MEMBER-REPORT-FILE ASSIGN TO "member.rpt"
              ORGANIZATION   IS LINE  SEQUENTIAL.

       
       DATA DIVISION. 
       FILE SECTION. 
       FD  MEMBER-FILE.
       01  MEMEBER-REC.
           88 END-OF-MEMBER-FILE   VALUE    HIGH-VALUE .
           05 MEMBER-ID   PIC X(5).
           05 MEMBER-NAME   PIC X(20).
           05 MEMBER-TYPE   PIC 9.
           05 MEMBER-GENDER   PIC X.
       FD  MEMBER-REPORT-FILE.
       01  PRN-LINE PIC   X(44).
       WORKING-STORAGE SECTION. 
       01  PAGE-HEADER.
           05 FILLER  PIC X(44)  VALUE "Rolling Greens Golf Club - Membe
      -    "rship Report".
       01  PAGE-FOOTING.
           05 FILLER PIC  X(15) VALUE SPACES.
           05 FILLER   PIC   X(7)  VALUE "PAGE : ".
           05 PRN-PAGE-NUM   PIC   Z9.
       01  COLUMN-HEADING PIC   X(41) VALUE 
           "MemberID    Member Name       Type Gender".
       01  MEMBER-DETAIL-LINE.
           05 FILLER PIC  X  VALUE   SPACE .
           05 PRN-MEMBER-ID  PIC   X(5).
           05 FILLTER  PIC   X(4)  VALUE SPACES.
           05 PRN-MEMBER-NAME   PIC   X(20).
           05 FILLTER  PIC   XX VALUE SPACES.
           05 PRN-MEMBER-TYPE   PIC   X.
           05 FILLTER  PIC   X(4) VALUE SPACES.
           05 PRN-MEMBER-GENDER   PIC   X.
       01  LINE-COUNT  PIC   99 VALUE ZEROS.
           88 NEW-PAGE-REQUIRED VALUE 40 THRU  99.
       01  PAGE-COUNT  PIC   99 VALUE ZEROS.





       PROCEDURE DIVISION .
       
       PROCESS-MEMBER-REPORT.
      *    เขียน
           OPEN  INPUT MEMBER-FILE
      *    อ่าน
           OPEN  OUTPUT MEMBER-REPORT-FILE 
      *    อ่านบรรทัดแรก
           PERFORM READ-MEMBER-REC 
      *    วนปริ้น1หน้า
           PERFORM  PRECESSS-PAGE UNTIL  END-OF-MEMBER-FILE 
           
           CLOSE MEMBER-FILE ,  MEMBER-REPORT-FILE 
           GOBACK 
           .


       READ-MEMBER-REC.
           READ MEMBER-FILE 
           AT END SET  END-OF-MEMBER-FILE   TO TRUE 
           END-READ 
           EXIT
           .

       PRECESSS-PAGE.
           PERFORM  WRITE-HEADING
           PERFORM  PROCESS-DETAIL UNTIL   NEW-PAGE-REQUIRED 
           OR END-OF-MEMBER-FILE 
           PERFORM  WRITE-FOOTER
           EXIT 
           .
      *    Rolling Greens Golf Club - Membership Report

      *    MemberID    Member Name       Type Gender 
       WRITE-HEADING.
           WRITE PRN-LINE FROM  PAGE-HEADER    AFTER ADVANCING PAGE .
      *    ปริ้นหลังจาก2บรรทัด
           WRITE PRN-LINE FROM  COLUMN-HEADING  AFTER ADVANCING 2 LINES
           COMPUTE  LINE-COUNT =   LINE-COUNT +   3
           COMPUTE PAGE-COUNT = PAGE-COUNT +  1
           MOVE  ZEROS TO LINE-COUNT 
           .
       
      *A1234    Teresa Casey          1    F
       PROCESS-DETAIL.
           MOVE  MEMBER-ID TO   PRN-MEMBER-ID 
           MOVE  MEMBER-NAME TO PRN-MEMBER-NAME 
           MOVE  MEMBER-TYPE TO PRN-MEMBER-TYPE 
           MOVE  MEMBER-GENDER TO  PRN-MEMBER-GENDER 
           WRITE PRN-LINE FROM  MEMBER-DETAIL-LINE 
                 AFTER ADVANCING 1 LINES
           COMPUTE  LINE-COUNT =   LINE-COUNT +   1
           PERFORM READ-MEMBER-REC 
           EXIT
       .

       WRITE-FOOTER.
           MOVE  PAGE-COUNT TO  PRN-PAGE-NUM 
           WRITE PRN-LINE FROM  PAGE-FOOTING AFTER ADVANCING 5  LINES           
           EXIT
           .

          

