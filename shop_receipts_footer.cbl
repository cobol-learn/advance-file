       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SHOP_RECEIPTS.
       AUTHOR. PAKAWAT JAROENYING.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT   SHOP-RECEIPT-FILE ASSIGN TO 
           "shop_receipts_footer.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
       

       DATA DIVISION. 
       FILE SECTION. 
       FD SHOP-RECEIPT-FILE.
      *HID006This is location of shop ID006
       01  SHOP-HEADER.
              88 END-OF-SHOP-RECEIPT-FILE   VALUE HIGH-VALUE .
           05 RECORD-TYPE-CODE  PIC X.
              88 IS-SHOP-HEADER VALUE "H".
              88 IS-SHOP-SALE   VALUE "S".
              88 IS-SHOP-FOOTER VALUE "F".
           05 SHOP-ID  PIC   X(5).
           05 SHOP-LOCATION  PIC   X(30).
      *SItemId0500500595
       01  SALE-RECEIPT.
           05 RECORD-TYPE-CODE  PIC X.
           05 ITEM-ID  PIC X(8).
           05 QTY-SOLD PIC   9(3).
           05 ITEM-COST   PIC   999V99.
       01  SHOP-FOOTER.
           05 RECORD-TYPE-CODE  PIC X.
           05 REC-COUNT   PIC  9(5).
       01  PRN-ERROR-MESSAGE.
           05 FILLER PIC  X(15) VALUE "Error on shop: ".
           05 PRN-ERROR-SHOP-ID PIC X(5).
           05 FILLER   PIC X(10)   VALUE "RCount : ".
           05 PRN-RECORD-COUNT  PIC   9(5).
           05 FILLER   PIC   X(10) VALUE "ACount : ".
           05 PRN-ACTUAL-COUNT  PIC   9(5).


       WORKING-STORAGE SECTION. 
       01  PRN-SHOP-SALE-TOTAL.
           05 FILLER PIC  X(21) VALUE "Total sales for shop".
           05 PRN-SHOP-ID PIC   X(5) .
           05 PRN-SHOP-TOTAL PIC   $$$$,$$9.99.
       01  SHOP-TOTAL  PIC   9(5)V99.
       01  ITEM-COUNT  PIC   9(5).

       PROCEDURE DIVISION.

       SHOP-SALES-PROCESS.
           OPEN INPUT SHOP-RECEIPT-FILE 
      *    อ่านบรรทัดแรก
           PERFORM READ-SHOP-RECEIPT-FILE
      *    วน Header  และทำจนกว่าจะจบ
           PERFORM PROCESS-SHOP-HEADER UNTIL END-OF-SHOP-RECEIPT-FILE
           CLOSE SHOP-RECEIPT-FILE
           GOBACK
       .

       PROCESS-SHOP-HEADER.
           MOVE  SHOP-ID  TO PRN-SHOP-ID 
           MOVE  ZEROS TO SHOP-TOTAL 
           MOVE ZEROS TO ITEM-COUNT 
      *    อ่านบรรทัด
           PERFORM READ-SHOP-RECEIPT-FILE 
      *    วน item จนกระทั่ง หมดบรรทัด หรือ มี header ใหม่
           PERFORM PROCESS-SHOP-ITEM UNTIL  END-OF-SHOP-RECEIPT-FILE 
                                            OR IS-SHOP-FOOTER  
           PERFORM PROCESS-SHOP-FOOTER
      *    อ่านอีกรอยบ
           PERFORM READ-SHOP-RECEIPT-FILE 
           EXIT 
       .

       PROCESS-SHOP-FOOTER.
           IF ITEM-COUNT = REC-COUNT 
              MOVE  SHOP-TOTAL TO  PRN-SHOP-TOTAL 
              DISPLAY PRN-SHOP-SALE-TOTAL 
           ELSE  
              MOVE  REC-COUNT TO   PRN-RECORD-COUNT 
              MOVE  ITEM-COUNT TO  PRN-ACTUAL-COUNT 
              MOVE  PRN-SHOP-ID TO PRN-ERROR-SHOP-ID 
              DISPLAY "ERROR ON SHOP: "PRN-ERROR-MESSAGE
           END-IF
           EXIT
       .

       PROCESS-SHOP-ITEM.
           COMPUTE SHOP-TOTAL = SHOP-TOTAL + (ITEM-COST  * QTY-SOLD )
           COMPUTE ITEM-COUNT = ITEM-COUNT + 1
           PERFORM READ-SHOP-RECEIPT-FILE 
           EXIT
           .

       READ-SHOP-RECEIPT-FILE.
           READ SHOP-RECEIPT-FILE 
           AT END SET  END-OF-SHOP-RECEIPT-FILE   TO TRUE 
           END-READ  
           EXIT
           .